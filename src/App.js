import './App.css'
import SocialMediaCardGreen from './components/SocialMediaCardGreen'
import SocialMediaCardRed from './components/SocialMediaCardRed'
import facebook from './public/img/icon-facebook.svg'
import instagram from './public/img/icon-instagram.svg'
import twitter from './public/img/icon-twitter.svg'
import youTube from './public/img/icon-youtube.svg'

function App() {
  return (
    <div className='App'>
      <div className='container-card-social'>
        <SocialMediaCardGreen imagePath={facebook} nameCard='@nathanf' classBorder='border-facebook' />
        <SocialMediaCardGreen imagePath={twitter} nameCard='@nathanf' classBorder='border-twitter' />
        <SocialMediaCardGreen imagePath={instagram} nameCard='@realnathanf' classBorder='border-instagram' />
        <SocialMediaCardRed imagePath={youTube} nameCard='Nathan F.' classBorder='border-youtube' />
      </div>
    </div>
  )
}

export default App
