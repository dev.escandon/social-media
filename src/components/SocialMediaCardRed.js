import './SocialMedia.css'
import arrowDown from '../public/img/icon-down.svg'

function SocialMediaCardRed(props) {
  return (
    <div className={`cards-social ${props.classBorder}`}>
      <div className='image-text'>
        <div>
          <img src={props.imagePath} />
        </div>
        <h2>{props.nameCard}</h2>
      </div>
      <p>1987</p>
      <h3> Followers</h3>
      <div className='image-followers'>
        <div>
          <img src={arrowDown} />
        </div>
        <h4 className='red-text-estatus'>12 Today</h4>
      </div>
    </div>
  )
}

export default SocialMediaCardRed
